#+TITLE: Outline for "Inside Straight"
#+AUTHOR: Matthew Graybosch

* "I know what you did in college."

- Brad Turner is at a consolation dinner thrown by his friends in the wake of his being denied an appointment to the Superior Court of New York.
- He was denied this appointment because the Phoenix Society investigated his background, giving particular attention to allegations that he took advantage of another student while she was drunk.
- While the Society didn't find sufficient evidence to prove guilt at a criminal trial, they found enough to justify vetoing Turner's appointment to SCONY. This is called a "not suitable" verdict, incidentally.
- As he's about to give a toast, he receives a message demanding that he leave the party lest he be gunned down in front of his friends and family.
- His demand for an explanation is met only with a taunt: "Justice will be denied no longer. I know what you did in college, and justice will be denied no longer."
- Turner steps outside, and finds a figure dressed in black save for a red scarf waiting for him. The figure throws a sheathed sword to Turner, and it falls at his feet. They then speak, using a synthesizer to distort their voice, and command Turner to pick up the sword, draw, and defend themselves.
- Turner refuses, but this does not faze the figure in black, who tells him that dying with a sword in his hand is better than he deserves before pulling a revolver with their free hand and shooting him.
- The shot hits Turner in the chest, piercing his right lung. It isn't enough to kill him outright, but the bullet to the head delivered at point-blank range finishes the job.